import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

//Used to define the base URL of all AJAX requests to the backend
//This is the URL where the Express server is listening for requests
//Made global to be used by the whole application
global.baseURL = 'http://localhost:3000';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
]

const router = new VueRouter({
  routes
});

export default router;
