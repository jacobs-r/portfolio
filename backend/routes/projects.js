var express = require('express');
const mongodb = require('mongodb');
const mongoClient = require('mongodb').MongoClient;
const url = process.env.DBURL;
var router = express.Router();

var options = {
    replSet: {
        sslValidate: false,
    }
}


router.get('/get', (req, res) => {
    mongoClient.connect(url, options, { useUnifiedTopology: true }, (err, db) => {
        if (err) throw err;
        let dbo = db.db('portfolio');
        dbo.collection('portfolio').find().toArray((error, result) => {
            if (error) throw error;
            db.close();
            res.send(JSON.stringify(result));
            res.end();
        });
    });
});

// copied from previous CRUD project; unnecessary for the portfolio, which just reads db
// router.post('/post', (req, res) => {
//     let formUser = req.body;
//     mongoClient.connect(url, options, { useUnifiedTopology: true }, (err, db) => {
//         if (err) throw err;
//         let dbo = db.db('clinic-manager');
//         let newUser = {
//             username: formUser.username,
//             password: formUser.password,
//             admin: formUser.admin,
//         }
//         dbo.collection('users').insertOne(newUser, (error, result) => {
//             if (error) throw error;
//             db.close();
//             res.end();
//         });
//     });
// });

// router.delete('/delete/:id', (req, res) => {
//     let id = req.params.id;
//     mongoClient.connect(url, options, { useUnifiedTopology: true }, (err, db) => {
//         if (err) throw err;
//         let dbo = db.db('clinic-manager');
//         let param1 = { _id: new mongodb.ObjectID(id) };
//         dbo.collection('users').deleteOne(param1, (error, result) => {
//             if (error) throw error;
//             db.close();
//             res.end();
//         });
//     });
// });

// router.put('/update/:id', (req, res) => {
//     let id = { _id: new mongodb.ObjectID(req.params.id) };
//     let formUser = req.body;
//     let updateInfo = {
//         $set: {
//             username: formUser.username,
//             password: formUser.password,
//             admin: formUser.admin,
//         }
//     }
//     console.log(updateInfo);
//     mongoClient.connect(url, options, { useUnifiedTopology: true }, (err, db) => {
//         if (err) throw err;
//         let dbo = db.db('clinic-manager');
//         dbo.collection('users').findOne(id, (error2, result2) => {
//             if (error2) throw error2;
//             dbo.collection('users').updateOne(id, updateInfo, (error, result) => {
//                 if (error) throw error;
//                 db.close();
//                 res.end();
//             });
//         })
//     });
// });

// router.post('/login', (req, res) => {
//     let user = req.body;
//     mongoClient.connect(url, options, { useUnifiedTopology: true }, (err, db) => {
//         if (err) throw err;
//         let dbo = db.db('clinic-manager');
//         dbo.collection('users').find({ username: user.username, password: user.password }).toArray((error, result) => {
//             if (error) throw error;
//             db.close();
//             res.end(JSON.stringify(result));
//         });
//     });
// });

module.exports = router;